# Introduction

## What is cloud?
![](assets/markdown-img-paste-20201116054430173.png)

* The delivery of computer services over the internet to assist with resource flexibility, scale, and innovation.
* These services include:
  * servers/VMs, 
  * physical storage,
  * data repositories,
  * networking resources,
  * analytics,
  * software, and more.
* These services are offered at a much smaller pay-for-use model than physical data centers and organizations generally migrate to the compute service model for the following reasons:
  * **Global** allows an organization to scale elastically and allow regional entry points, providing a more consistent experience for their user base
  * **Speed** allows the faster provision of resources and helps release pressure on capacity planning because it removed the need to order and wait for resources
  * **Cost** is simplified into a pay-as-you-go model, removing the capital expense over the physical resources
  * **Security** is provided through an extensive set of policies and controls that strengthen your overall security posture
  * **Productivity** is increased by removing a significant number of tasks, such as hardware setup, patching, and other operational management chores
  * **Performance** is achieved through worldwide distribution of services and is regularly updated to the latest generation of computing hardware

# What type of clouds are there?
Cloud computing comes in private, public, and hybrid types, with each offering its own benefits:

* **Public** clouds are generally owned by third parties such as Amazon, Microsoft, and Google, who control all the supporting infrastructure, hardware, and software
* **Private** clouds are owned by a business and maintained on a private network
* **Hybrid** clouds are combinations of public and private, as well as on-premise resources

# What problems does it solve?
The biggest benefit of the cloud is `it allows organizations to expand without a large capital cost`, and also `allows new/small businesses to shrink their startup or capital into a pay-as-you-go model`. The cloud allows for innovation to happen without the traditional fears of infrastructure purchases, giving the ability to bring up and tear down in a rapid and cost effective way with little concern for the infrastructure needed.

Think of it as `an unlimited playground where the only true limit is your imagination`. You can now build, migrate, or rebuild your applications, so you truly only have to worry about the application and not about everything around it. So, the problem it solves is allowing your organization to scale faster, while controlling cost and offline responsibilities to help streamline IT operations.

## Capital expenditure (CAPEX) versus operating expenses (OPEX)
* **CAPEX** refers to a large upfront spend of money used to get an asset (an asset is a resource that will yield benefits over time, not just in the current period)
* **OPEX** refers to smaller, recurring spends of money for current period benefit

![](assets/markdown-img-paste-20201116055416126.png)

If you use the cloud, you don't need to make the big upfront payment. That in turn has several associated advantages:
* **No depreciation**: When you buy a house, hopefully, it appreciates, but when you buy a car, it loses a fourth of its value as soon as you drive it out of the dealer's parking lot. Depreciation is called an intangible expense, but try selling a barely used car, and you will find it tangible enough. With the cloud, you don't worry about depreciation, but the cloud provider does.
* **Transparency**: Let's face it folks—big contracts are complicated and have been known to have big payoffs for people concerned. This is a reality of doing business: procurement and sourcing are messy businesses for a reason. Using the cloud is far more transparent and simpler for internal processes and audit
* **Flexibility in capacity planning**: The worlds of business and history are littered with unfulfilled ambitions and unrealistic plans and targets that went nowhere. Your firm might have a target to triple revenues in 3 years: such ambitious plans are common enough. If you, as an architect, sign off on a commensurate tripling in your server capacity and that business growth does not happen, the finance guys will come asking you why overspent if you are still in this firm 3 years down the line. At that point, you will likely not have the luxury of pointing a finger at the CEO who launched the 3X plan. He likely will have moved on adroitly to another shiny plan.

# What is the difference between on premise and cloud?

## Deployment model
* **On Premises**: In an on-premise environment, resources are deployed in-house and within an enterprise’s IT infrastructure. An `enterprise is responsible for maintaining the solution` and all its related processes.
* **Cloud**: While there are different forms of cloud computing (such as public cloud, private cloud, and a hybrid cloud), in a public cloud computing environment, `resources are hosted on the premises of the service provider` but enterprises are able to access those resources and use as much as they want at any given time.

### Cost of services
* **On Premises**: For enterprises that deploy software on premise, they are responsible for the ongoing costs of the server hardware, power consumption, and space.
* **Cloud**: Enterprises that elect to use a cloud computing model `only need to pay for the resources that they use`, with none of the maintenance and upkeep costs, and the price adjusts up or down depending on how much is consumed.

### Controlling of services
* **On Premises**: In an on-premises environment, enterprises retain all their data and can `fully control what happens` to it, for better or worse. Companies in highly regulated industries with extra privacy concerns are more likely to hesitate to leap into the cloud before others because of this reason.
* **Cloud**: In a cloud computing environment, the question of ownership of data is one that many companies have struggled with. Data and encryption keys reside within your third-party provider, so if some unexpected happens and there is downtime, they may unable to access their data.

### Security concerns
* **On Premises**: Companies that have extra sensitive information, such as government and banking industries must have a certain level of security and `privacy that an on-premises environment provides`. Despite the promise of the cloud, security is the primary concern for many industries, so an on-premises environment, despite some if its drawbacks and price tag, make more sense.
* **Cloud**: Security concerns remain the number one barrier to a cloud computing deployment. There have been many publicized cloud breaches, and IT departments around the world are concerned.

### Compliance
* **On Premises**: Many companies these days operate under some form of regulatory control, regardless of the industry. Perhaps the most common one is the Health Insurance Portability and Accountability Act (HIPAA) for private health information, but there are many others, including the Family Educational Rights and Privacy Act (FERPA), which contains detailed student records, and other government and industry regulations. For companies that are subject to such regulations, are always have to know where their data stored to remain compliant.
* **Cloud**: Enterprises that do choose a cloud computing model must do their due diligence and `ensure that their third-party provider compliant` with all the different regulatory mandates within their industry.

# Autoscaling and autohealing

The technical rationale for moving to the cloud can often be summed up in two words: **autoscaling** and **autohealing**.

* **Autoscaling**: The idea of autoscaling is simple enough although the implementations can get quite involved—apps are deployed on compute, the amount of compute capacity increases or decreases depending on the level of incoming client requests. In a nutshell, all the public cloud providers have services that make autoscaling and autohealing easily available. Autoscaling, in particular, is a huge deal. Imagine a large Hadoop cluster, with say 1,000 nodes. Try scaling that; it probably is a matter of weeks or even months. You'd need to get and configure the machines, reshard the data and jump through a trillion hoops. With a cloud provider, you'd simply use an elastic version of Hadoop such as Dataproc on the GCP or Elastic MapReduce (EMR) on AWS and you'd be in business in minutes. This is not some marketing or sales spiel; the speed of scaling up and down on the cloud is just insane.

* **Autohealing**: The idea of autohealing is just as important as that of autoscaling, but it is less explicitly understood. Let's say that we deploy an app that could be a Java JAR, Python package, or Docker container to a set of compute resources, which again could be cloud VMs, App Engine backends, or pods in a Kubernetes cluster. Those compute resources will have problems from time to time; they will crash, hang, run out of memory, throw exceptions, and misbehave in all kinds of unpredictable ways. If we did nothing about these problems, those compute resources would effectively be out of action, and our total compute capacity would fall and, sooner or later, become insufficient to meet client requests. So, clearly, we need to somehow detect whether our compute resources got sick, and then heal them. In the pre-cloud days, this would have been pretty manual, some poor sap of an engineer would have to nurse a bare metal or VM back to health. Now, with cloud-based abstractions, individual compute units are much more expendable. We can just take them down and replace them with new ones. Because these units of compute capacity are interchangeable (or fungible—a fancier word that means the same thing), autohealing is now possible.
