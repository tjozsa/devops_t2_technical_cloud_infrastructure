# Intro to Azure

![](assets/markdown-img-paste-2020111707305430.png)

## What is Azure?
* Azure is Microsoft’s cloud offering, which comes in private, on-premise (Azure Stack), government, and public versions (refer to this link for further details https://azure.microsoft.com/en-us/overview/what-is-a-private-cloud/).
* Released on February 2010
* The next evolution of virtualization.

The magic of Azure is built on top of a technology called **Service Fabric** and **Fabric controllers**
* a distributed application system
* handles the allocation of servers or services,
* monitors the health of these,
* and heals them as needed.

Each Fabric Controller orchestrates all the resources needed within the Azure platform.

All the network resources and servers have been abstracted out of your view and there is no longer a need to support and maintain these resources.

This will create what is referred to as a **closed ecosystem**, and Azure offers its services in this closed ecosystem for mass consumption.

This closed ecosystem allows its consumers to not have to worry about the underlying technology and, in most cases, the operating system needs to support that technology. This has helped in abstracting away the need to support the many facets of technology in today's organizations.

## Cloud Architecture

### Components of "eating a pizza for dinner"
![](assets/markdown-img-paste-20201116064116219.png)

![](assets/markdown-img-paste-20201116064128862.png)

![](assets/markdown-img-paste-20201116064146229.png)

![](assets/markdown-img-paste-20201116064204714.png)

**Notice**:
* We can get the same pizza in any discussed way (in theory*)
* The choice depending only on our needs and aspects
* It is the providers choice which parts of the stack they manage
* The name of a service is just a convention, offerings of the providers may vary

### Components of a cloud infrastructure
* Application (what the end user see)
* Runtime/middleware (java, python etc.)
* Operating system
* Virtualization
* Network
* Hardware

![](assets/markdown-img-paste-20201116064338903.png)

![](assets/markdown-img-paste-20201116064352162.png)

![](assets/markdown-img-paste-20201116064503856.png)

![](assets/markdown-img-paste-2020111606451942.png)

![](assets/markdown-img-paste-20201116064548352.png)

![](assets/markdown-img-paste-20201116064557145.png)

![](assets/markdown-img-paste-2020111606461032.png)

![](assets/markdown-img-paste-2020111606462331.png)

## Azure Cloud Architecture
With the adoption of the cloud, there has come new guidance on the usage of resource types, cost, as well as deployment needs. These needs and deployment strategies differ by a cloud provider, but one I have been very accustomed to us is Azure.  

Azure provides **Platform as a service (PaaS)**, **Software as a service (SaaS)**, and **Infrastructure as a service (IaaS)** for a large variety of programming languages.

One of the biggest hurdles in your journey beside which type of resources to use is the cost associated with spinning up that type of resource.

Let’s take a moment to look at the responsibility to understand what responsibilities you are keeping and are releasing Microsoft, in following figure:

![](assets/markdown-img-paste-20201116063450744.png)

Now that we understand the responsible parties on the type of resources we pick, let’s dive into Azure and see how to leverage it to run solutions.

# Administration methods on Azure
* Provider Web UI
  * Easy to learn, but slow to work with; not convenient for production
* (Cloud) CLI
  * Harder to learn, scriptable!
* Infrastructure as a Code (IaC)
  * Several external tools available such as Ansible, Terraform and Cloud Formation

# User Administration on Azure

![](assets/markdown-img-paste-2020111606465656.png)

![](assets/markdown-img-paste-20201116064704482.png)

![](assets/markdown-img-paste-20201116064712568.png)

# Words about Azure Pricing
Basically we are paying for:
* Hardware:
  * CPU
  * RAM
  * Storage
* Networking
  * Interfaces, traffic
  * Network components
* Software
  * OS/Runtime
  * Installed applications

![](assets/markdown-img-paste-20201116065018520.png)

![](assets/markdown-img-paste-20201116065028691.png)

![](assets/markdown-img-paste-20201116065038915.png)

![](assets/markdown-img-paste-20201116065048825.png)

![](assets/markdown-img-paste-20201116065059868.png)

# Azure portal with built in Powershell or Bash

![](assets/markdown-img-paste-20201116084006752.png)

Upon first run it will ask you to create a storage account.

![](assets/markdown-img-paste-20201116083956592.png)
