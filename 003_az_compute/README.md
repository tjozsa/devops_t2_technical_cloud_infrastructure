# VMs the Compute in Azure IaaS
You can create VMs in the Azure Portal manually.

**Select Virtual machines**

![](assets/markdown-img-paste-20201116075821225.png)

**Click Add**

![](assets/markdown-img-paste-20201116075829850.png)

**You can select VM size**

![](assets/markdown-img-paste-20201116075836683.png)

Maybe it is the good time to go through various VM sizing guides and the Azure Price Calculator.
* [VM Size naming conventions](https://docs.microsoft.com/en-us/azure/virtual-machines/vm-naming-conventions)
* [VM Sizes by purpose](https://docs.microsoft.com/en-us/azure/virtual-machines/sizes)
* [What size means in terms of power](https://docs.microsoft.com/en-us/azure/cloud-services/cloud-services-sizes-specs)
* [General purpose](https://docs.microsoft.com/en-us/azure/virtual-machines/sizes-general)
* [Azure Price Calculator](https://azure.microsoft.com/en-us/pricing/calculator/)

**Choose Open Ports**

![](assets/markdown-img-paste-2020111608052598.png)

**Generate SSH Keypair**

![](assets/markdown-img-paste-20201116080559680.png)

**After several minutes**

![](assets/markdown-img-paste-20201116075923387.png)

# Another way is to re-use an ARM template

Take a look at the deployment you just made and discuss what ARM templates are meant to be for.

Also here is an example that shows various components of a template based deployment:

## Basic layout of ARM templates

```json
{
 "$schema": "http://schema.management.azure.com/schemas/2015-01-01/deploymentTemplate.json#",
 "contentVersion": "",
 "parameters": { },
 "variables": { },
 "functions": { },
 "resources": [ ],
 "outputs": { }
}
```

![](assets/markdown-img-paste-20201116080846285.png)

## How to deploy Azure Resource group with ARM template
Function to check if we need to log in
```
function Check-Login
{
    $needLogin = $true
    Try
    {
        $content = Get-AzureRmContext
        if ($content)
        {
            $needLogin = ([string]::IsNullOrEmpty($content.Account))
        }
    }
    Catch
    {
        if ($_ -like "*Login-AzureRmAccount to login*")
        {
            $needLogin = $true
        }
        else
        {
            throw
        }
    }
    if ($needLogin)
    {
        Login-AzureRmAccount
    }
}
Check-Login
```

The ARM template

```json
{
  "$schema": "http://schema.management.azure.com/schemas/2015-01-01/deploymentParameters.json#",
  "contentVersion": "1.0.0.0",
  "parameters": {
    "adminUsername": {
      "value": "azureuser"
    },
    "adminPassword": {
      "value": "Azure12345678"
    },
    "PrivateSubnet": {
      "value": "10.0.1.0/24"
    }
  }
}
```

Of course templates get more complicated, so they can be broken up into several files.

Of course templates are the means of reuse so they should be templated and parameter values for the current run should be supplied.
