# Get your trial account for free
1. Go to [https://cloud.google.com/](https://cloud.google.com/) and click `Get started for free`

![](assets/markdown-img-paste-20201117053847723.png)

2. Now, it will ask you to login to your Gmail account and choose your country and accept the terms & conditions.

![](assets/markdown-img-paste-20201117054005964.png)

 ![](assets/markdown-img-paste-20201117054040363.png)

3. In the third step, you have to fill your details, like account type, Name, Address, credit card details, tax information, etc. If you have old Gmail account and all the information is already there it would take it and you might not have to fill all the details.

![](assets/markdown-img-paste-20201117054142923.png)

![](assets/markdown-img-paste-20201117054157575.png)

4. start using GCP

![](assets/markdown-img-paste-20201117054222687.png)
