# Intro to GCP

![](assets/markdown-img-paste-20201117072919161.png)

[More Here](https://cloud.google.com/about/locations)

Google Cloud Platform == ``**GCP**``
* a collection of products
* allows the world to use some of Google’s internal infrastructure
* includes many things that are common across all cloud providers:
  * such as on-demand virtual machines via Google Compute Engine
  * or object storage for storing files via Google Cloud Storage
* also includes APIs to Google-built technology:
  * like Bigtable,
  * Cloud Datastore,
  * or Kubernetes

## Go and explore the GCP console
[https://console.cloud.google.com](https://console.cloud.google.com)

## Explore Pricing
[https://cloud.google.com/products/calculator](https://cloud.google.com/products/calculator)

## Explore Firebase
[https://firebase.google.com/](https://firebase.google.com/)

# Documentation
[GCP Docs](https://cloud.google.com/docs)
