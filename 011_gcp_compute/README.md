# Getting started with GCP Compute

[GCP machine types](https://cloud.google.com/compute/docs/machine-types)

[GCP images](https://cloud.google.com/compute/docs/images)

[A word on Microsoft Software](https://cloud.google.com/compute/docs/instances/windows)

## Instance template

An instance template is a resource that you can use to create Virtual Machine (VM) instances and managed instance groups (MIGs).

Instance templates define the machine type, boot disk image or container image, labels, and other instance properties. You can then use an instance template to create a MIG or to create individual VMs.

Use instance templates any time you want to quickly create VM instances based off of a preexisting configuration. If you want to create a group of identical instances, you must use an instance template to create a MIG.

[What are Instance templates](https://cloud.google.com/compute/docs/instance-templates)

[Create Instance Templates](https://cloud.google.com/compute/docs/instance-templates/create-instance-templates)

## What about disk?
Compute Engine offers several types of storage options for your instances. Each of the following storage options has unique price and performance characteristics:

* Zonal persistent disk: Efficient, reliable block storage.
* Regional persistent disk: Regional block storage replicated in two zones.
* Local SSD: High performance, transient, local block storage.
* Cloud Storage buckets: Affordable object storage.
* Filestore: High performance file storage for Google Cloud users.

If you are not sure which option to use, the most common solution is to add a persistent disk to your instance.

[More on this here](https://cloud.google.com/compute/docs/disks)

## Are there Spot instances on GCP?
A preemptible VM is an instance that you can create and run at a much lower price than normal instances. However, Compute Engine might stop (preempt) these instances if it requires access to those resources for other tasks. Preemptible instances are excess Compute Engine capacity, so their availability varies with usage.

[Preemptible VM instances](https://cloud.google.com/compute/docs/instances/preemptible)

### Preemptible instance limitations
Preemptible instances function like normal instances but have the following limitations:

* Compute Engine might stop preemptible instances at any time due to system events. The probability that Compute Engine will stop a preemptible instance for a system event is generally low, but might vary from day to day and from zone to zone depending on current conditions.
* Compute Engine always stops preemptible instances after they run for 24 hours. Certain actions reset this 24-hour counter.
* Preemptible instances are finite Compute Engine resources, so they might not always be available.
* Preemptible instances can't live migrate to a regular VM instance, or be set to automatically restart when there is a maintenance event.
* Due to the above limitations, preemptible instances are not covered by any Service Level Agreement (and, for clarity, are excluded from the Compute Engine SLA).
* The Google Cloud Free Tier credits for Compute Engine do not apply to preemptible instances.
