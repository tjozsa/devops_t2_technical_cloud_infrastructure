# GCP Database Solutions

## Cloud Bigtable
Store terabytes or petabytes of data using a NoSQL wide-column database service.

[More Here](https://cloud.google.com/bigtable/docs)
## Firestore
Add NoSQL document database access to mobile and web apps.

[More Here](https://cloud.google.com/firestore/docs)
## Memorystore
Achieve extreme performance using a managed in-memory data store service.

[More Here](https://cloud.google.com/memorystore/docs/redis)
## Cloud Spanner
Back your apps with a mission-critical, global-scale, relational database service.

[More Here](https://cloud.google.com/spanner/docs)
## Cloud SQL
Add MySQL, PostgreSQL, and SQL Server database services to your apps.

[More Here](https://cloud.google.com/sql/docs)
## Firebase Realtime Database
Store and sync data in real time.

[More Here](https://firebase.google.com/docs/database/)
## Database Migration Service
Serverless, easy, minimal downtime migrations to Cloud SQL.

[More Here](https://cloud.google.com/database-migration/docs)
