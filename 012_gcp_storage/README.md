# GCP Storage Options

[Start here](https://cloud.google.com/products/storage)

## Cloud Storage
Object storage for companies of all sizes. Store any amount of data. Retrieve it as often as you’d like.

* Unlimited storage with no minimum object size
* Worldwide accessibility and worldwide storage locations
* Low latency
* High durability (99.999999999% annual durability)
* Geo-redundancy if the data is stored in a multi-region or dual-region

[More here](https://cloud.google.com/storage)

## Filestore
High-performance, fully managed file storage.

* Fully managed network attached storage (NAS) for Compute Engine and GKE instances
* Predictable price for performance
* Scales to 100s of TBs for high-performance workloads

[More here] (https://cloud.google.com/filestore)
