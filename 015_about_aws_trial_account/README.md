# Get your "FREE" AWS account

1. [Go to https://aws.amazon.com/free](https://aws.amazon.com/free)
![](assets/markdown-img-paste-2020111706120796.png)

Examine all the free tier services you will get access to for limited time and credits worth.

2. Click "Create a Free Account"

3. Create an AWS account

![](assets/markdown-img-paste-20201117061328561.png)

4. Provide your Address, Credit Card details, Phone number ...

5. Log in to the AWS console.

![](assets/markdown-img-paste-20201117061449683.png)
