# Compute Services: AWS EC2 Instances

At AWS, virtual servers are called Elastic Compute Cloud (EC2) instances. At the most basic level, each instance is a virtual server running a version of Windows or Linux. Instances run as a guest operating system hosted by the hypervisor, which in turn is directly installed on a bare-metal server. AWS first launched instances back in August 2006, and there was just one size: the M1.

 > There are more than 150 instance choices available at AWS.

Each instance is configured at launch with a select amount of memory, vCPU cores, storage, and networking bandwidth. An instance can be sliced and diced in many different patterns and configurations. Regardless of the instance size and type, each configuration has several mandatory parts:

![](assets/markdown-img-paste-20201117073416828.png)

The following components are part of each configuration:
* Built by preconfigured images called Amazon machine images
* Authenticated using a unique public/private key pair per instance
* Storage options of persistent block storage and temporary storage volumes
* A mandatory firewall called a Security Group that protects your network interfaces
* Basic or enhanced network adapters
* Shared or dedicated hardware options

## INSTANCE FAMILIES

For each instance’s name, the first letter that you see is the instance family that it belongs to. This letter describes the resources allocated to the instance and the workloads that the instance is best suited for. The letter also stands for something; for example, the letter C stands for compute, R for RAM, and I for input/output per second (IOPS).

![](assets/markdown-img-paste-20201117073511766.png)

## WHAT’S A VCPU?
AWS defines the amount of CPU power assigned to each instance as a virtual CPU (vCPU). A vCPU is a part of a physical CPU core. A process called hyperthreading associates two virtual threads to each physical core—an A and a B thread working in a multitasking mode:

![](assets/markdown-img-paste-20201117073638500.png)

The required core count of your on-premise applications is something to check for before they are migrated to AWS

> To get a listing of the actual core count, visit https://aws.amazon.com/ec2/virtualcores.

## EC2 INSTANCE CHOICES

The EC2 console has more than 150 instance types to choose:
* from general purpose
* to instances designed for:
  * compute,
  * storage,
  * and memory-optimized workloads

![](assets/markdown-img-paste-20201117073729220.png)

[More Here](https://aws.amazon.com/ec2/instance-types/)

## Calculator

[https://calculator.aws/#/](https://calculator.aws/#/)
